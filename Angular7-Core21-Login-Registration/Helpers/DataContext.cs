﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Angular7_Core21_Login_Registration.Entities;
using Microsoft.EntityFrameworkCore;

namespace Angular7_Core21_Login_Registration.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
    }
}
