﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Angular7_Core21_Login_Registration.Dtos;
using Angular7_Core21_Login_Registration.Entities;
using AutoMapper;

namespace Angular7_Core21_Login_Registration.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
